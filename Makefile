CC = gcc
CXX = g++
STRIP = strip
__TARGET = yuvsplit
TARGET = $(TARGET)

ifeq ($(OS),Windows_NT)
TARGET = $(__TARGET).exe
endif
all:
	$(CXX) main.cpp -o $(TARGET) -Wall -O3 -static
	$(STRIP) $(TARGET)
