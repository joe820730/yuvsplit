#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cctype>
#include <cstdint>

int main(int argc, char **argv)
{
  if(argc != 8)
  {
    printf("Usage: %s [filename] [width] [height] [colorspace] [start_frame] [end_frame] [output_name]\n", argv[0]);
    printf("Support colorspace: YUV420, Y\n");
    printf("\nAuthor: Joe.\n");
    printf("sizeof = %llu\n", sizeof(long long unsigned int));
    return 0;
  }
  FILE *fpIn = nullptr, *fpOut = nullptr;
  int32_t imgWidth = 0, imgHeight = 0, startFrame = 0, endFrame = 0;
  imgWidth = atoi(argv[2]);
  imgHeight = atoi(argv[3]);
  startFrame = atoi(argv[5])-1;
  endFrame = atoi(argv[6])-1;
  fpIn = fopen(argv[1], "rb");
  fpOut = fopen(argv[7], "wb");
  if(fpIn == nullptr)
  {
    fprintf(stderr, "File %s not exist.\n", argv[1]);
    return -1;
  }
  if(fpOut == nullptr)
  {
    fprintf(stderr, "Can't craeate output file!\n");
    return -1;
  }
  if(imgWidth == 0 || imgHeight == 0)
  {
    fprintf(stderr, "Don't set height or width to zero!\n");
    return -1;
  }
  if(startFrame < 0)
    startFrame = 0;
  if(startFrame > endFrame)
  {
    fprintf(stderr, "End frame: %d is behind start frame: %d !!\n", endFrame, startFrame);
    return -1;
  }
  std::string colorSpace = std::string(argv[4]);
  int32_t frameSize = 0;
  if(colorSpace == "YUV420")
  {
    frameSize = imgHeight*imgWidth*3/2;
  }
  else if(colorSpace == "Y")
  {
    frameSize = imgHeight*imgWidth;
  }
  else
  {
    fprintf(stderr, "Unknown color space %s!\n", argv[4]);
    return -1;
  }
  fseeko64(fpIn, 0, SEEK_END);
  int64_t fileSize = ftello64(fpIn);
  printf("filesize = %lld\n", fileSize);
  int64_t seekStart = (int64_t)startFrame * (int64_t)frameSize;
	printf("seekStart = %lld\n", seekStart);
  int64_t totalFrame = fileSize/frameSize;
  if(seekStart >= fileSize || seekStart < 0)
  {
    fprintf(stderr, "Start frame out of range: %lld/%lld\n", seekStart, totalFrame);
    return -1;
  }
  fseeko64(fpIn, seekStart, SEEK_SET);
  uint8_t *fpInbuf = new uint8_t[frameSize];
  int32_t curFrame = startFrame;
  while(curFrame < endFrame && !feof(fpIn))
  {
    printf("Current frame: %d/%d\r", curFrame+1, endFrame);
    fread(fpInbuf, sizeof(uint8_t), frameSize, fpIn);
    fwrite(fpInbuf, sizeof(uint8_t), frameSize, fpOut);
    curFrame++;
  }
  fclose(fpOut);
  fclose(fpIn);
  delete [] fpInbuf;
  printf("\nDone!\n");
  return 0;
}
